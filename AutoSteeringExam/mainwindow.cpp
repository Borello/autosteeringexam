#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <vector>
#include <math.h>
#include <QFileDialog>
#include <QString>

#include "accelerometerreading.h"
#include "rollandpitchestimation.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Initializing ui elements
    QPalette p(palette());
    p.setColor(QPalette::Background, Qt::lightGray);
    this->ui->frame->setAutoFillBackground(true);
    this->ui->frame->setPalette(p);
    this->ui->plotFrame->hide();
    this->resize(380,455);
    this->plotted = false;
    this->chart = new QChart();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_inputFileNameButton_clicked()
{
    //Set input file name
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open File"),"./",tr("Text files (*.txt *.log *.dat)"));
    this->ui->inputFileName->setText(fileName);

    //Set frame's background color
    QPalette p(palette());
    p.setColor(QPalette::Background, Qt::lightGray);
    this->ui->frame->setAutoFillBackground(true);
    this->ui->frame->setPalette(p);
}

void MainWindow::on_outputFileNameButton_clicked()
{
    //Set output file name
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open File"),"./",tr("Text files (*.txt *.log *.dat)"));
    this->ui->outputFileName->setText(fileName);

    //Set frame's background color
    QPalette p(palette());
    p.setColor(QPalette::Background, Qt::lightGray);
    this->ui->frame->setAutoFillBackground(true);
    this->ui->frame->setPalette(p);
}

void MainWindow::on_pushButton_clicked()
{
    if ((this->ui->inputFileName->text() != "") && (this->ui->outputFileName->text() != ""))
    {
        //Creating timeseries of accelerometer readings and estimations
        std::vector<AccelerometerReading> accelerometerTimeSerie;
        std::vector<RollAndPitchEstimation> rollAndPitchTimeSerie;

        //Reading log archive
        FILE *inputFile;
        inputFile = fopen(this->ui->inputFileName->text().toStdString().c_str(),"r");

        long int timestamp;
        int gpx, gpy, gpz;
        float conversionScale = 1.0/1024; //Accelerometer conversion scale for 14bit resolution and -+8g range
        if (inputFile)
        {
            while (true)
            {
                fscanf(inputFile,"%ld; %d; %d; %d",&timestamp,&gpx,&gpy,&gpz);
                if (fgetc(inputFile) == EOF){
                    break;
                }
                //Ading accelerometer reading to timeserie
                accelerometerTimeSerie.push_back(AccelerometerReading(timestamp,gpx*conversionScale,gpy*conversionScale,gpz*conversionScale));
            }
            fclose(inputFile);
        }
        else
        {
            qDebug() << "Could not read the input file...";
        }


        //Estimating Roll and Pitch
        int index;
        for (index = 0; index < accelerometerTimeSerie.size(); index++)
        {
            rollAndPitchTimeSerie.push_back(RollAndPitchEstimation(accelerometerTimeSerie[index]));
        }


        //Saving Results
        FILE *outputFile;
        outputFile = fopen(this->ui->outputFileName->text().toStdString().c_str(),"w");
        if (outputFile)
        {
            for (index = 0; index < rollAndPitchTimeSerie.size(); index++)
            {
                fprintf(outputFile,"%ld; %f; %f\n",rollAndPitchTimeSerie[index].getTimestamp(),rollAndPitchTimeSerie[index].getCompensatedRoll(),rollAndPitchTimeSerie[index].getPitch());
            }
            fclose(outputFile);
        }
        else
        {
            qDebug() << "Could not read the input file...";
        }

        //Set frame's background color
        QPalette p(palette());
        p.setColor(QPalette::Background, Qt::green);
        this->ui->frame->setAutoFillBackground(true);
        this->ui->frame->setPalette(p);

        //Creating Plot Data
        QLineSeries *rollSeries = new QLineSeries();
        QLineSeries *pitchSeries = new QLineSeries();
        QLineSeries *compensatedRollSeries = new QLineSeries();
        QLineSeries *zeroSeries = new QLineSeries();
        for (index = 0; index < rollAndPitchTimeSerie.size(); index++)
        {
            rollSeries->append(rollAndPitchTimeSerie[index].getTimestamp()-rollAndPitchTimeSerie[0].getTimestamp(),rollAndPitchTimeSerie[index].getRoll());
            pitchSeries->append(rollAndPitchTimeSerie[index].getTimestamp()-rollAndPitchTimeSerie[0].getTimestamp(),rollAndPitchTimeSerie[index].getPitch());
            compensatedRollSeries->append(rollAndPitchTimeSerie[index].getTimestamp()-rollAndPitchTimeSerie[0].getTimestamp(),rollAndPitchTimeSerie[index].getCompensatedRoll());
            zeroSeries->append(rollAndPitchTimeSerie[index].getTimestamp()-rollAndPitchTimeSerie[0].getTimestamp(),0);
        }

        //Creating and managing Plot widget
        this->chart->removeAllSeries();
        rollSeries->setName("Roll");
        pitchSeries->setName("Pitch");
        compensatedRollSeries->setName("Compensated Roll");
        zeroSeries->setColor(QColor(Qt::black));
        zeroSeries->setPen(QPen(Qt::DashLine));
        this->chart->addSeries(zeroSeries);
        this->chart->addSeries(rollSeries);
        this->chart->addSeries(pitchSeries);
        this->chart->addSeries(compensatedRollSeries);
        this->chart->createDefaultAxes();
        this->chart->setTitle("Roll and Pitch Estimations [rad]x[ms]");
        if (!this->plotted)
        {
            QChartView *chartView = new QChartView(this->chart);
            chartView->setRenderHint(QPainter::Antialiasing);
            QVBoxLayout *layout = new QVBoxLayout();
            layout->addWidget(chartView);
            this->ui->plotFrame->setLayout(layout);
            this->ui->plotFrame->show();
            this->plotted = true;
        }
    }
}
