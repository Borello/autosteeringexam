#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
using namespace QtCharts;
#include <QLineSeries>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_inputFileNameButton_clicked();

    void on_outputFileNameButton_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    bool plotted;
    QChart *chart;
};

#endif // MAINWINDOW_H
