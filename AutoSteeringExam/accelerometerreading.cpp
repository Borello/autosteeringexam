#include "accelerometerreading.h"
#include <math.h>

AccelerometerReading::AccelerometerReading(long int timestamp, float Gpx, float Gpy, float Gpz)
{
    this->timestamp = timestamp;
    this->Gpx = Gpx;
    this->Gpy = Gpy;
    this->Gpz = Gpz;
    this->module = sqrt(pow(Gpx,2)+pow(Gpy,2)+pow(Gpz,2));
}

AccelerometerReading::~AccelerometerReading(){}

long int AccelerometerReading::getTimestamp()
{
    return this->timestamp;
}

float AccelerometerReading::getGpx()
{
    return this->Gpx;
}

float AccelerometerReading::getGpy()
{
    return this->Gpy;
}

float AccelerometerReading::getGpz()
{
    return this->Gpz;
}

float AccelerometerReading::getModule()
{
    return this->module;
}
