#-------------------------------------------------
#
# Project created by QtCreator 2017-11-06T18:45:57
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutoSteeringExam
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    accelerometerreading.cpp \
    rollandpitchestimation.cpp

HEADERS  += mainwindow.h \
    accelerometerreading.h \
    rollandpitchestimation.h

FORMS    += mainwindow.ui

DISTFILES += \
    attitude_exam.log
