#ifndef ROLLANDPITCHESTIMATION_H
#define ROLLANDPITCHESTIMATION_H

#include "accelerometerreading.h"

class RollAndPitchEstimation
{
    public:
        RollAndPitchEstimation(AccelerometerReading currentReading);
        ~RollAndPitchEstimation();
        long int getTimestamp();
        float getRoll();
        float getPitch();
        float getCompensatedRoll();

    private:
        long int timestamp;
        float roll;
        float pitch;
        float compensatedRoll;
};

#endif // ROLLANDPITCHESTIMATION_H
