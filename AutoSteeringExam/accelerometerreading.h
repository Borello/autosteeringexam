#ifndef ACCELEROMETERREADING_H
#define ACCELEROMETERREADING_H

class AccelerometerReading
{
    public:
        AccelerometerReading(long int timestamp, float Gpx, float Gpy, float Gpz);
        ~AccelerometerReading();
        long int getTimestamp();
        float getGpx();
        float getGpy();
        float getGpz();
        float getModule();

    private:
        long int timestamp;
        float Gpx;
        float Gpy;
        float Gpz;
        float module;
};

#endif // ACCELEROMETERREADING_H
