#include "rollandpitchestimation.h"
#include <math.h>

RollAndPitchEstimation::RollAndPitchEstimation(AccelerometerReading currentReading)
{
    //Do all estimations
    this->roll = atan2(currentReading.getGpy(),currentReading.getGpz());
    this->pitch = atan(-currentReading.getGpx()/sqrt(pow(currentReading.getGpy(),2)+pow(currentReading.getGpz(),2)));
    this->timestamp = currentReading.getTimestamp();
    this->compensatedRoll = atan2( currentReading.getGpy() , (currentReading.getGpz() < 0 ? -1 : 1)*sqrt(pow(currentReading.getGpz(),2)+0.01*pow(currentReading.getGpx(),2)) );
}

RollAndPitchEstimation::~RollAndPitchEstimation(){}

long int RollAndPitchEstimation::getTimestamp()
{
    return this->timestamp;
}

float RollAndPitchEstimation::getRoll()
{
    return this->roll;
}

float RollAndPitchEstimation::getPitch()
{
    return this->pitch;
}

float RollAndPitchEstimation::getCompensatedRoll()
{
    return this->compensatedRoll;
}
