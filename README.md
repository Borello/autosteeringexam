#Hexagon Agriculture - Auto Steering Exam

Project developed for Hexagon Agriculture admission process

Author: Gilberto Solis Borello Guimar�es

#Assignment 1: Preparing the Development Environment:

##Task:

1. "Set up a development environment to develop a program in C++, ..."
2. "... use a version control tool"

##Solution:

Version control tool: git

Development Environment specifications:

* OS - Ubuntu Gnome;

* Framework - QT;

* IDE - QT Creator;

* Compiler - GCC.

#Assignment 2: Attitude Estimation Exam - Auto Steering:

##Tasks:

1. Read a file with raw accelerometer measurements;
2. Estimate Roll and Pitch angles and save in a output file;
3. Provide a suitable documentation.

##Solution:

The developed solution is a c++ based software that uses basic linear algebra to estimate the roll and pitch orientation of a vehicle provided with a three axis accelerometer.
The program has a graphical interface, developed on Qt Framework, that makes its usage more intuitive and also permits a better visualization of the output data, by plotting them in a graphic.
The output file is composed by three columns, with the following format:

"timestamp in ms";"roll estimation in radians"; "pitch estimation in radians"

#Usage:

###Linux version:
1. Extract the HexagonExamLinux.zip into a folder;
2. Open a terminal and go to the folder HexagonExam;
3. Execute the command: ./AutoSteeringExam

###Windows version:
1. Extract the HexagonExamWindows.zip into a folder;
2. Open the folder HexagonExam;
3. Execute the file AutoSteeringExam.exe by double-clicking it;

####For all:
4. Inside the program the user can select the input and output files (extensions: .log,.dat e .txt) or just write their names with extensions;
5. Then, he can press the button Run, saving the estimations in the output file and plotting the resulting data.

###About the results:
In the graphics are plotted: an estimation for the pitch orientation, an estimation for the roll angle using the exact linear algebra equation and an estimation for the roll angle using a less precise but more robust equation. 
The blue line represents the estimation of roll using the exact linear algebra equation. Althought mathematicaly precise this equation has a stability boundary, the roll estimation tends to infinity when Gpz tends to 0. This can be a critical problem for real systems. Because of that, it is proposed a compensation using the Gpx multiplied by a factor mu (equals to 0.01 in this program). The comparison between the plots demostrate clearly that this factor prevents the roll estimation of overflowing and also reduces noise. In the output file is printed only the robust estimation for the roll angle.




